/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wwc.poperationnel.web.rest.vm;
